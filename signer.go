package main

import (
	"sort"
	"strconv"
	"strings"
	"sync"
)

func ExecutePipeline(jobs ...job) {
	wg := &sync.WaitGroup{}
	in := make(chan interface{})

	for _, j := range jobs {
		wg.Add(1)
		out := make(chan interface{})
		go startJob(j, in, out, wg)
		in = out
	}
	wg.Wait()
}

func startJob(job job, in, out chan interface{}, wg *sync.WaitGroup) {
	defer wg.Done()
	job(in, out)
	close(out)
}

func CombineResults(in, out chan interface{}) {
	var resHash []string
	for i := range in {
		resHash = append(resHash, i.(string))
	}
	sort.Strings(resHash)
	out <- strings.Join(resHash, "_")
}

func SingleHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	mu := &sync.Mutex{}

	for i := range in {
		wg.Add(1)
		input := i
		go func(in interface{}, out chan interface{}, wg *sync.WaitGroup, mu *sync.Mutex) {
			crc32Data := ""
			toHash := strconv.Itoa(input.(int))
			go func() {
				crc32Data = DataSignerCrc32(toHash)
			}()
			mu.Lock()
			hashMD5 := DataSignerMd5(toHash)
			mu.Unlock()
			hashCRC32MD5 := DataSignerCrc32(hashMD5)
			out <- crc32Data + "~" + hashCRC32MD5
			wg.Done()
		}(in, out, wg, mu)
	}

	wg.Wait()
}

func MultiHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	for i := range in {
		wg.Add(1)
		go func(data string, out chan interface{}, wg *sync.WaitGroup) {
			defer wg.Done()
			mu := &sync.Mutex{}
			wgHashes := &sync.WaitGroup{}
			hashSlice := make([]string, 6)
			for i := 0; i < 6; i++ {
				wgHashes.Add(1)
				hash := strconv.Itoa(i) + data
				go changeSliceRaceSafe(hashSlice, hash, i, wgHashes, mu)
			}
			wgHashes.Wait()
			out <- strings.Join(hashSlice, "")
		}(i.(string), out, wg)
	}
	wg.Wait()
}

func changeSliceRaceSafe(hashSlice []string, hash string, i int,  wg *sync.WaitGroup, mu *sync.Mutex) {
	defer wg.Done()
	hash = DataSignerCrc32(hash)
	mu.Lock()
	hashSlice[i] = hash
	mu.Unlock()
}